<?php
	
	if(isset($_FILES['image'])){
     
      $file_name = $_FILES['image']['name'];
      $file_size =$_FILES['image']['size'];
      $file_tmp =$_FILES['image']['tmp_name'];
      $file_type=$_FILES['image']['type'];
      $extensions= array("jpeg","jpg","png");
      move_uploaded_file($file_tmp,"uploads/".$file_name);
      $filename = "uploads/".$file_name;
	 
	 
	  list($width, $height) = getimagesize($filename);
	  $new_width = 1024;
	  $new_height = $height * 1024 / $width;
	  $image_p = imagecreatetruecolor($new_width, $new_height);
	  $image = imagecreatefromjpeg($filename);
	  imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

	  
	  $stamp = imagecreatefrompng('wm.png');
	  $marge_right = 10;
      $marge_bottom = 10;
      $sx = imagesx($stamp);
	  $sy = imagesy($stamp);
	  imagecopy($image_p, $stamp, imagesx($image_p) - $sx - $marge_right, imagesy($image_p) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

	  imagejpeg($image_p, "converted/".$file_name);
	  
	  $path = "converted/".$file_name;
	  $id = 25;
	  
	  $db = new SQLite3('fars_database');
	  $query = "INSERT INTO files(ID, URL) VALUES ('$id', '$path')";
      $db->query($query);
	  
	  
	 }
   
  
?>

<html>
   <body>
      
      <form action="" method="POST" enctype="multipart/form-data">
         <input type="file" name="image" />
         <input type="submit"/>
      </form>
      
   </body>
</html>